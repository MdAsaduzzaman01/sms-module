﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SMSService.Controllers;


namespace TestSmsModule
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            BulkSMSController controller = new BulkSMSController();
            // Act
            ViewResult result = controller.SmsListPartial(1, null, null) as ViewResult;
            // Assert
            Assert.IsNotNull(result);


        }
    }
}
