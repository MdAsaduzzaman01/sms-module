﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SMSService.Models;

namespace SMSService.Reports
{
    public class CsvReports
    {
        public bool ListToCsv(List<BulkSMSNumberList> list,string fileName)
        {
            try
            {
                if (list.Any())
                {
                    var sb = new StringBuilder();
                    foreach (var data in list)
                    {
                        sb.AppendLine(data.MobileNumber);
                    }
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    //System.IO.File.WriteAllText(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NumberLisr.txt"),sb.ToString());
                    System.IO.File.WriteAllText(System.IO.Path.Combine(path, fileName + ".txt"), sb.ToString());

                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
           
        }


   


}//cs
}//ns