﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace SMSService.ViewModel
{
    internal class SentSmsVM
    {
        public string MobileName { get; set; }
        public string Message { get; set; }
        public string SMSSource { get; set; }
        public int IsSend { get; set; }
        public string EntryDate { get; set; }
        public string SendingTime { get; set; }
    }//cls
}//ns