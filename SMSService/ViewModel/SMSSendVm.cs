﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMSService.ViewModel
{
    public class SMSSendVm
    {
        public int ID { get; set; }
        public string MobileName { get; set; }
        public string Message { get; set; }
        public string SMSSource { get; set; }
        public int Issend { get; set; }
        public string Entrydate { get; set; }
        public string SendingTime { get; set; }
    }
}