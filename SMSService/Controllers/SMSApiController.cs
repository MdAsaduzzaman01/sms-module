﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using SMSService.Models;

namespace SMSService.Controllers
{
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class SmsApiController : ApiController
    {

        [System.Web.Http.HttpPost]
        public void SendSmsInsert([FromBody] SendSM sendSms)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    db.SendSMS.Add(sendSms);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [System.Web.Http.HttpGet]
        public JsonResult<List<SendSmsVm>> GetData()
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    List<SendSmsVm> m=new List<SendSmsVm>();
                    List<SendSM> da = db.SendSMS.ToList();
                    foreach (var i in da)
                    {
                        SendSmsVm b =new SendSmsVm();
                        b.MobileName= i.MobileName;
                        b.Message = i.Message;
                        b.Entrydate = i.Entrydate;
                        b.SendingTime = i.SendingTime;
                        b.SMSSource = i.SMSSource;
                        b.Issend = i.Issend;
                        var firstOrDefault = db.smsCodeFlags.FirstOrDefault(x => x.smscodeID == i.Issend);
                        if (firstOrDefault != null)
                        {
                            var codeFlagName = firstOrDefault.SmsCodeName;
                            b.codeFlagName = codeFlagName;
                        }
                        m.Add(b);

                    }

                    return Json(m);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAllData(int id)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var getData = db.SendSMS.FirstOrDefault(x => x.ID == id);
                    if (getData != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, getData);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "SMS With Id" + " " + id.ToString() + " " + " Not Found");
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }//cls

    public class SendSmsVm
    {
        public int ID { get; set; }
        public string MobileName { get; set; }
        public string Message { get; set; }
        public string SMSSource { get; set; }
        public int Issend { get; set; }
        public string codeFlagName { get; set; }
        public string Entrydate { get; set; }
        public string SendingTime { get; set; }
    }
}//ns
