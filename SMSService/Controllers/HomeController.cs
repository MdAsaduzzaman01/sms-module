﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.Mvc;
using SMSService.Models;

namespace SMSService.Controllers
{
    public class HomeController : Controller
    {


        //=============================================View Page==========================================

        public ActionResult MainDashboard()
        {
            return View();
        }
        //=============================================View Page End======================================

        //=============================================Partial View Page==================================
        //=============================================Partial View Page End==============================

        //=============================================Jason Action=======================================
        [HttpGet]
        public ActionResult SmsDashboard()
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {

                    MainDashboardVm model = new MainDashboardVm();

                    var totalSent = db.SendSMS.Where(x => x.Issend == 2).ToList();
                    model.TotalSentSms = totalSent.Any() ? totalSent.Count() : 0;

                    var totalNotSent = db.SendSMS.Where(x => x.Issend == 1).ToList();
                    model.TotalNotSentSms = totalNotSent.Any() ? totalNotSent.Count() : 0;

                    var totalErrorSms = db.SendSMS.Where(x => x.Issend == 3).ToList();
                    model.TotalErrorSms = totalErrorSms.Any() ? totalErrorSms.Count() : 0;

                    var lastSentSmsList = db.SendSMS.Where(x => x.Issend == 2).ToList();
                    if (lastSentSmsList.Any()) { 
                    var lastSentSms = lastSentSmsList.LastOrDefault();
                        if (lastSentSms != null)
                        {
                            model.MobileNumber = lastSentSms.MobileName;
                            model.Message = lastSentSms.Message;
                            if (lastSentSms.SendingTime != "")
                            {
                                model.SendingDate = Convert.ToDateTime(lastSentSms.SendingTime);
                            }
                           
                        }
                    }
                    List<GroupNameVm> gnList=new List<GroupNameVm>();
                  
                    var bsList = db.BulkSMSListNames.ToList();
                    foreach (var i in bsList)
                    {
                        GroupNameVm gn = new GroupNameVm();
                        var listName = bsList.FirstOrDefault(x => x.ListNameID == i.ListNameID).ListName;
                        gn.ListNames = listName.Any()? listName:"";
                        gn.TotalMobileNumbers = db.BulkSMSNumberLists.Where(x => x.ListNameID == i.ListNameID).Count();
                        gnList.Add(gn);
                    }
                    model.GroupList = gnList;
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //=============================================Jason Action End===================================

        //=============================================Post  Method======================================
        //=============================================Post  Method======================================

        //=============================================Get  Method=======================================
        //=============================================Get  Method=======================================

        //=============================================Edit  Method======================================
        //=============================================Edit  Method End==================================

        //=============================================Update  Method====================================
        //=============================================Update  Method End================================

        //=============================================Delete  Method====================================
        //=============================================Delete  Method End================================



    }//controller

    internal class MainDashboardVm
    {
       
        public int TotalSentSms { get; set; }
        public int TotalNotSentSms { get; set; }
        public int TotalErrorSms { get; set; }
        public string MobileNumber { get; set; }
        public DateTime SendingDate { get; set; }
        public string Message { get; set; }
        public List<GroupNameVm> GroupList { get; set; }
    }//cls vm
    internal class GroupNameVm
    {
        public string ListNames { get; set; }
        public int TotalMobileNumbers { get; set; }
       
    }//cls vm
}