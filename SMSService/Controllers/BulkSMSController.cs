﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using SMSService.Models;
using SMSService.ViewModel;
using static System.Data.Entity.Core.Objects.EntityFunctions;

namespace SMSService.Controllers
{
    public class BulkSMSController : Controller
    {


        //=============================================View Page==========================================
        public ActionResult BulkSmsList()
        {
            return View();
        }


        public ActionResult BulkSMSNumbers()
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    ViewBag.ListName = db.BulkSMSListNames.ToList();
                    return View();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public ActionResult SmsList()
        {
            return View();
        }

        public ActionResult SendSMS()
        {

            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    ViewBag.BSList = db.BulkSMSListNames.ToList();
                    return View();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        //=============================================View Page End======================================

        //=============================================Partial View Page==================================

        public ActionResult LoadBulkSmsListName()
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var BulkSmsNameList = db.BulkSMSListNames.ToList();
                    return PartialView("_LoadBulkSmsListName", BulkSmsNameList);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }




        public ActionResult SmsListPartial(int isSend,DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var rDate = DateTime.Today;
                    TimeSpan time = new TimeSpan(23, 59, 59);
                    
                    //DateTime nFDate;
                    //DateTime nTDate;
                    
                    if (fromDate != null && toDate != null)
                    {
                        DateTime check = (DateTime)toDate;
                        fromDate = (DateTime) fromDate;
                        toDate = check.Add(time);
                    }
                    else
                    {
                        fromDate = rDate;
                        toDate = rDate.Add(time);
                    }

                    string sql = "select * from [dbo].[SendSMS] where Issend="+ isSend + " and Entrydate between CONVERT(date,'"+ fromDate + "') and CONVERT(date,'"+ toDate + "')";
                    var data = db.Database.SqlQuery<SMSSendVm>(sql).ToList();
                    return PartialView("_SmsListPartial", data);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        //=============================================Partial View Page End==============================

        //=============================================Jason Action=======================================
        //=============================================Jason Action End===================================

        //=============================================Post  Method======================================

        
        public ActionResult DeleteListNameAndNumber(int id)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var lId = db.BulkSMSListNames.Find(id);
                   var listOfNumber = db.BulkSMSNumberLists.Where(x => x.ListNameID == id).ToList();
                    if (listOfNumber.Any())
                    {
                        db.BulkSMSNumberLists.RemoveRange(listOfNumber);
                        db.SaveChanges();
                    }
                    if (lId != null) db.BulkSMSListNames.Remove(lId);
                    db.SaveChanges();
                   
                    return Json("List Deleted successfully", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        [HttpPost]
        public bool ResendSms(int id)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var resendValue = db.SendSMS.FirstOrDefault(x => x.ID == id);
                    resendValue.Issend = 1;
                    db.SaveChanges();
                   return true;
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        [HttpPost]

        public bool ErrorDeleteData(int id)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var valueOfRowDel = db.SendSMS.Find(id);
                    if (valueOfRowDel != null) db.SendSMS.Remove(valueOfRowDel);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }   
        }
        [HttpPost]
        public ActionResult BulkSmsListEntry(string listName, string btnSave, int? listNameId)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    if (btnSave == "Submit")
                    {
                        if (!string.IsNullOrWhiteSpace(listName))
                        {

                            bool data = db.BulkSMSListNames.Where(x => x.ListName == listName).Any();

                            if (data)
                            {
                                TempData["ErrorMassage"] = "List Name Already Exist";
                            }
                            else
                            {

                                BulkSMSListName bslm = new BulkSMSListName();
                                bslm.ListName = listName;
                                bslm.EntryDate = DateTime.Now.ToString();
                                db.BulkSMSListNames.Add(bslm);
                                db.SaveChanges();
                                TempData["successMassage"] = "Data Saved Successfully!";
                            }

                        }
                        else
                        {
                            TempData["ErrorMassage"] = "Data not Saved! Please try again";
                        }
                    }
                    else if (btnSave == "Update")
                    {
                        if (listNameId != null)
                        {
                            var bSLName = db.BulkSMSListNames.FirstOrDefault(x => x.ListNameID == listNameId);
                            var b = db.BulkSMSListNames.Where(x => x.ListName == listName).Any();
                            if (b)
                            {
                                TempData["ErrorMassage"] = "List Name Already Exist";

                            }
                            else
                            {
                                bSLName.ListName = listName;
                                db.SaveChanges();
                                TempData["successMassage"] = "Data Saved Successfully!";

                            }

                        }
                    }

                    return RedirectToAction("BulkSmsList");

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [HttpPost]
        public ActionResult BulkSMSNumberListEntry(int? listNameFId, int? listNameNId, HttpPostedFileBase smsNumberFile, string mobileNumber = "")
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {

                    if (smsNumberFile != null && listNameFId!=null)
                    {
                        var filePath = "";
                        string fileExtension = Path.GetExtension(smsNumberFile.FileName).ToLower();
                        if (fileExtension == ".txt" || fileExtension == ".csv")
                        {
                            string path = Server.MapPath("~/Uploads/");
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            var file = path + Path.GetFileName(smsNumberFile.FileName);
                            filePath = file;
                            smsNumberFile.SaveAs(filePath);
                            string fileData = System.IO.File.ReadAllText(filePath);
                            foreach (string row in fileData.Split('\n'))
                            {
                                if (!string.IsNullOrEmpty(row))
                                {
                                    var mobNumber = "";
                                    var rowData = row.Split(',').ToList();


                                    if (fileExtension == ".csv")
                                    {
                                        for (int i = 0; i < rowData.Count; i++)
                                        {
                                            // check if mobile Number isExist
                                            if (rowData[i] == "\r")
                                                continue;
                                            mobNumber = "0" + rowData[i];
                                            var moNuList = db.BulkSMSNumberLists.Where(x => x.ListNameID == listNameFId).ToList();
                                            var data = moNuList.Any(x => x.MobileNumber == mobNumber);
                                            if (data)
                                                continue;

                                            if (!string.IsNullOrWhiteSpace(mobNumber) && mobNumber.Length >= 11)
                                            {
                                                string justDigits = new string(mobNumber.Where(char.IsDigit).ToArray());
                                                if (justDigits.Length >= 11)
                                                {
                                                    mobNumber = justDigits.Substring(justDigits.Length - 11, 11);                                                    
                                                }
                                            }

                                            BulkSMSNumberList bsn = new BulkSMSNumberList();
                                            bsn.MobileNumber = mobNumber.Trim();
                                            bsn.EntryDate = DateTime.Now.ToString();
                                            bsn.ListNameID = (int)listNameFId;
                                            db.BulkSMSNumberLists.Add(bsn);
                                            db.SaveChanges();

                                        }
                                        
                                    }

                                    else
                                    {
                                        for (int i = 0; i < rowData.Count; i++)
                                        {
                                            if (rowData[i] == "\r")
                                                continue;

                                            string mNum = rowData[i];
                                            var moNuList = db.BulkSMSNumberLists.Where(x => x.ListNameID == listNameFId && x.MobileNumber == mNum).ToList();

                                            if (moNuList.Any())
                                                continue;
                                            if (!string.IsNullOrWhiteSpace(mNum) && mNum.Length >= 11)
                                            {
                                                string justDigits = new string(mNum.Where(char.IsDigit).ToArray());
                                                if (justDigits.Length >= 11)
                                                {
                                                    mNum = justDigits.Substring(justDigits.Length - 11, 11);
                                                }
                                            }

                                            BulkSMSNumberList bsn = new BulkSMSNumberList();
                                            bsn.MobileNumber = mNum;
                                            bsn.EntryDate = DateTime.Now.ToString();
                                            bsn.ListNameID = (int)listNameFId;
                                            db.BulkSMSNumberLists.Add(bsn);
                                            db.SaveChanges();

                                        }
                                        
                                    }
                                    TempData["successMassage"] = "Data Saved Successfully!";
                                }
                            }
                        }
                        System.IO.File.Delete(filePath);
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(mobileNumber) && listNameNId != null)
                        {
                            var moNuList = db.BulkSMSNumberLists.Where(x => x.ListNameID == listNameNId).ToList();
                            var data = moNuList.Any(x => x.MobileNumber == mobileNumber);
                            if (data)
                            {
                                TempData["ErrorMassage"] = "List Name Already Exist";
                                return RedirectToAction("BulkSMSNumbers");
                            }
                            else
                            {
                                BulkSMSNumberList bsn = new BulkSMSNumberList();
                                bsn.MobileNumber = mobileNumber;
                                bsn.EntryDate = DateTime.Now.ToString();
                                bsn.ListNameID = (int)listNameNId;
                                db.BulkSMSNumberLists.Add(bsn);
                                db.SaveChanges();
                                TempData["successMassage"] = "Data Saved Successfully!";
                            }
                        }
                    }

                    return RedirectToAction("BulkSMSNumbers");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SingleSmsEntry(string sMobileNumber, string sMessage)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    if (!string.IsNullOrWhiteSpace(sMobileNumber) && !string.IsNullOrWhiteSpace(sMessage))
                    {
                        // var codeFlagId = db.smsCodeFlags.FirstOrDefault(x => x.SmsCodeName == "Not Send").smscodeID;
                        SendSM ss = new SendSM();
                        ss.MobileName = sMobileNumber;
                        ss.Message = sMessage;
                        ss.SMSSource = "Single SMS";
                        ss.Issend = 1;
                        ss.Entrydate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                        ss.SendingTime = String.Empty;
                        db.SendSMS.Add(ss);
                        db.SaveChanges();
                        TempData["successMassage"] = "Data Saved Successfully!";
                        return RedirectToAction("SendSMS");
                    }
                    TempData["ErrorMassage"] = "Data not Saved! Please try again";
                    return RedirectToAction("SendSMS");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult BulkSmsEntry(string bulkMessageBody, string smsListArryHidden)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var SmSList = JsonConvert.DeserializeObject<List<bulkSmSListVm>>(smsListArryHidden);
                    if (SmSList.Any())
                    {
                        foreach (var sl in SmSList)
                        {
                            var bulk = db.BulkSMSNumberLists.Where(x => x.ListNameID == sl.Id).ToList();
                            foreach (var b in bulk)
                            {
                                SendSM sm = new SendSM();
                                sm.MobileName = b.MobileNumber;
                                sm.Message = bulkMessageBody ?? "";
                                sm.SMSSource = sl.Name;
                                sm.Issend = 1;
                                sm.Entrydate = DateTime.Now.ToString();
                                sm.SendingTime = String.Empty;
                                db.SendSMS.Add(sm);
                                db.SaveChanges();
                            }


                        }
                        TempData["successMassage"] = "Data Saved Successfully!";
                        return RedirectToAction("SendSMS");
                    }
                    TempData["ErrorMassage"] = "No Data Found! Please try again";
                    return RedirectToAction("SendSMS");
                }

            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
        //=============================================Post  Method======================================

        //=============================================Get  Method=======================================
        [HttpGet]
        public JsonResult EditList(int id)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    var model = (from data in db.BulkSMSListNames
                                 where data.ListNameID == id
                                 select new
                                 {
                                     listname = data.ListName,
                                     listId = data.ListNameID
                                 }).FirstOrDefault();
                    return Json(model, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //=============================================Get  Method=======================================

        //=============================================Edit  Method======================================
        //=============================================Edit  Method End==================================

        //=============================================Update  Method====================================
        //=============================================Update  Method End================================

        //=============================================Delete  Method====================================
        //=============================================Delete  Method End================================





    }//cls
}//ns