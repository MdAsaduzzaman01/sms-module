﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMSService.Models;
using SMSService.Reports;

namespace SMSService.Controllers
{
    public class GroupListController : Controller
    {
        // GET: GroupList

        //=============================================View Page==========================================
        public ActionResult GroupNamewiseList()
        {

            using (SMSServiceEntities db = new SMSServiceEntities())
            {
                try
                {
                    ViewBag.GroupName = db.BulkSMSListNames.ToList();
                    return View();
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
        //=============================================View Page End======================================
        public ActionResult GroupNamewiseListPartialView(int listNameId)
        {

            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    List<BulkSMSNumberList> model = new List<BulkSMSNumberList>();
                    model = GetGroupNumberList(listNameId);
                    return PartialView("_GroupNamewiseListPartialView", model);


                }


            }
            catch (Exception ex)
            {

                throw ex;
            }



        }
        public ActionResult GroupNamewiseListEditPartialView(int id)
        {

            try
            {
                SMSServiceEntities db = new SMSServiceEntities();

                BulkSMSNumberList model = db.BulkSMSNumberLists.Find(id);
                if (model != null)
                {
                    ViewBag.ListName = db.BulkSMSListNames.ToList();


                }
                return PartialView("_GroupNamewiseListEditPartialView", model);





            }
            catch (Exception ex)
            {

                throw ex;
            }



        }
        //=============================================Partial View Page==================================

        //=============================================Partial View Page End==============================

        //=============================================Jason Action=======================================
        public ActionResult DownloadList(int listNameId, string type)
        {

            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    bool result = false;
                    var name = "";
                    var bulkSmsListName = db.BulkSMSListNames.FirstOrDefault(x => x.ListNameID == listNameId);
                    if (bulkSmsListName != null)
                    {
                         name = bulkSmsListName.ListName??"";
                    }
                    List<BulkSMSNumberList> model = new List<BulkSMSNumberList>();
                    model = GetGroupNumberList(listNameId);
                    if (model.Any())
                    {
                        if (type == "Excel")
                        {
                            ExcelReports ep = new ExcelReports();
                           
                            if (ep.ListToExcel(model))
                            {
                                return Json("", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                Type officeType = Type.GetTypeFromProgID("Excel.Application");
                                if (officeType == null)
                                {
                                    return Json("You have no MS-Excel installed!", JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json("Try Again!", JsonRequestBehavior.AllowGet);
                                }
                            }
                            

                        }
                        else if(type == "CSV")
                        {
                            CsvReports cs=new CsvReports();
                            if (cs.ListToCsv(model, name))
                            {
                                return Json("Download Successfully Completed! See Your Desktop.", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("Try Again!", JsonRequestBehavior.AllowGet);
                            }
                        }


                        return Json("", JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json("No data available!", JsonRequestBehavior.AllowGet);
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }



        }

        //=============================================Jason Action End===================================

        //=============================================Post  Method======================================
        //=============================================Post  Method======================================

        //=============================================Get  Method=======================================
        public List<BulkSMSNumberList> GetGroupNumberList(int listNameId)
        {
            try
            {
                using (SMSServiceEntities db = new SMSServiceEntities())
                {
                    List<BulkSMSNumberList> model = new List<BulkSMSNumberList>();

                    if (listNameId > 0)
                    {
                        var ml = db.BulkSMSNumberLists.Where(x => x.ListNameID == listNameId).ToList();
                        foreach (var l in ml)
                        {
                            BulkSMSNumberList gl = new BulkSMSNumberList();
                            gl.ID = l.ID;
                            gl.MobileNumber = l.MobileNumber;
                            gl.EntryDate = l.EntryDate;
                            gl.ListNameID = l.ListNameID;
                            model.Add(gl);
                        }
                    }

                    return model;

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }


        }//
        //=============================================Get  Method=======================================

        //=============================================Edit  Method======================================
        public ActionResult UpdateList(BulkSMSNumberList model)
        {
            using (SMSServiceEntities db = new SMSServiceEntities())
            {
                try
                {
                    BulkSMSNumberList m = db.BulkSMSNumberLists.Find(model.ID);
                    if (m != null)
                    {
                        m.ListNameID = model.ListNameID;
                        m.MobileNumber = model.MobileNumber;
                        m.EntryDate = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                        db.SaveChanges();
                    }
                    return RedirectToAction("GroupNamewiseList");

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
        //=============================================Edit  Method End==================================

        //=============================================Update  Method====================================
        public ActionResult GroupItemDelete(int id)
        {
            using (SMSServiceEntities db = new SMSServiceEntities())
            {

                try
                {
                    BulkSMSNumberList find = db.BulkSMSNumberLists.Find(id);
                    if (find != null)
                    {
                        db.BulkSMSNumberLists.Remove(find);
                        db.SaveChanges();
                        return Json("Remove Successfully!", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Item Not Found! Try Again.", JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
        //=============================================Update  Method End================================

        //=============================================Delete  Method====================================
        //=============================================Delete  Method End================================
    }
}